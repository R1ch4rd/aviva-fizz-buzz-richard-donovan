﻿using Domain;
using Domain.Repositories;

namespace Repository
{
    public class FizzBuzzItemRepository : IFizzBuzzItemRepository
    {
        //// ----------------------------------------------------------------------------------------------------------
		 
        public IFizzBuzzItem Create(int number)
        {
            return new FizzBuzzItem(number);
        }

        //// ----------------------------------------------------------------------------------------------------------
    }
}