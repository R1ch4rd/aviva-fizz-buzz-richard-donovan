﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Repository.Tests
{
    [TestClass]
    public class FizzBuzzItemRepositoryTest
    {
        //// ----------------------------------------------------------------------------------------------------------
		 
        [TestMethod]
        public void Create_ValueOfOne_ExpectResultNotNull()
        {
            // Arrange
            const int Value = 1;
            var fizzBuzzItemRepository = new FizzBuzzItemRepository();

            // Act
            var item = fizzBuzzItemRepository.Create(Value);

            // Assert
            Assert.IsNotNull(item);
            Assert.AreEqual(Value, item.Value);
        }

        //// ---------------------------------------------------------------------------------------------------------- 
    }
}