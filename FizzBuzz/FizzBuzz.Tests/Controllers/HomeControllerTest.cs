﻿using Domain;
using Domain.Repositories;

using FizzBuzz.Controllers;
using FizzBuzz.Models;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Rhino.Mocks;

namespace FizzBuzz.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        //// ----------------------------------------------------------------------------------------------------------

        private MockRepository mocks;

        //// ----------------------------------------------------------------------------------------------------------

        [TestInitialize]
        public void TestInitialize()
        {
            mocks = new MockRepository();
        }

        //// ----------------------------------------------------------------------------------------------------------

        [TestCleanup]
        public void TestCleanUp()
        {
            mocks.VerifyAll();
        }

        //// ----------------------------------------------------------------------------------------------------------

        [TestMethod]
        public void Index_ModelIsHasNotBeenInitialized_ExpectModelIsNull()
        {
            // Arrange
            var fakeFizzBuzzItemRepository = mocks.StrictMock<IFizzBuzzItemRepository>();

            var controller = GetHomeController(fakeFizzBuzzItemRepository);

            mocks.ReplayAll();
            var model = new FizzBuzzModel();

            // Act
            var result = controller.Index(model);

            //Assert
            Assert.IsNotNull(result);
            Assert.IsNull(result.ViewData.Model);
        }

        //// ----------------------------------------------------------------------------------------------------------

        [TestMethod]
        public void Index_ValueThree_ExpectThreeItems()
        {
            // Arrange
            const int Value = 3;
            const int ExpectedItemCount = 3;

            var fakeFizzzBuzzItemRepository = PrepareFizzBuzzItemRepository(Value);
            var controller = GetHomeController(fakeFizzzBuzzItemRepository);
            var model = new FizzBuzzModel { Value = Value };

            mocks.ReplayAll();

            // Act
            var result = controller.Index(model);
            var fizzBuzzItems = (result.ViewData.Model as FizzBuzzModel).FizzBuzzItems;

            // Assert
            Assert.AreEqual(ExpectedItemCount, fizzBuzzItems.Count);
        }

        //// ----------------------------------------------------------------------------------------------------------

        [TestMethod]
        public void Index_ValueFive_ExpectFiveItems()
        {
            // Arrange
            const int Value = 5;
            const int ExpectedItemCount = 5;

            var fakeFizzzBuzzItemRepository = PrepareFizzBuzzItemRepository(Value);
            var controller = GetHomeController(fakeFizzzBuzzItemRepository);
            var model = new FizzBuzzModel { Value = Value };

            mocks.ReplayAll();

            // Act
            var result = controller.Index(model);
            var fizzBuzzItems = (result.ViewData.Model as FizzBuzzModel).FizzBuzzItems;

            // Assert
            Assert.AreEqual(ExpectedItemCount, fizzBuzzItems.Count);
        }

        //// ----------------------------------------------------------------------------------------------------------

        [TestMethod]
        public void Index_ValueFifteen_ExpectFifteenItems()
        {
            // Arrange
            const int Value = 15;
            const int ExpectedItemCount = 15;

            var fakeFizzzBuzzItemRepository = PrepareFizzBuzzItemRepository(Value);
            var controller = GetHomeController(fakeFizzzBuzzItemRepository);
            var model = new FizzBuzzModel { Value = Value };

            mocks.ReplayAll();

            // Act
            var result = controller.Index(model);
            var fizzBuzzItems = (result.ViewData.Model as FizzBuzzModel).FizzBuzzItems;

            // Assert
            Assert.AreEqual(ExpectedItemCount, fizzBuzzItems.Count);
        }

        //// ----------------------------------------------------------------------------------------------------------

        private IFizzBuzzItemRepository PrepareFizzBuzzItemRepository(int value)
        {
            var fakeFizzBuzzItemRepository = mocks.StrictMock<IFizzBuzzItemRepository>();

            for (int i = 1; i <= value; i++)
                Expect.Call(fakeFizzBuzzItemRepository.Create(i)).Return(new FizzBuzzItem(i));

            return fakeFizzBuzzItemRepository;
        }

        //// ----------------------------------------------------------------------------------------------------------

        private HomeController GetHomeController(IFizzBuzzItemRepository fizzBuzzItemRepository)
        {
            return new HomeController(fizzBuzzItemRepository);
        }
    }
}