﻿using System;

using Domain.Enums;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Domain.Tests
{
    [TestClass]
    public class FizzBuzzItemTest
    {
        //// ----------------------------------------------------------------------------------------------------------
		 
        [TestMethod]
        public void Constructor_ValueOfOne_TypeEqualsNumber()
        {
            // Arrange
            var fizzBuzzItem = GetFizzBuzzItem(1);

            // Act

            //Asset
            Assert.AreEqual(FizzBuzzItemType.Number, fizzBuzzItem.Type);
        }

        //// ----------------------------------------------------------------------------------------------------------

        [TestMethod]
        public void Constructor_ValueDisibleByThree_TypeEqualsFizz()
        {
            // Arrange
            const int Value = 3;
            var fizzBuzzItem = GetFizzBuzzItem(Value);

            // Act

            //Asset
            Assert.AreEqual(FizzBuzzItemType.Fizz, fizzBuzzItem.Type);
        }

        //// ----------------------------------------------------------------------------------------------------------

        [TestMethod]
        public void Constructor_ValueDivisibleByOnlyFive_TypeEqualsBuzz()
        {
            // Arrange
            const int Value = 5;
            var fizzBuzzItem = GetFizzBuzzItem(Value);

            // Act

            // Assert
            Assert.AreEqual(fizzBuzzItem.Type, FizzBuzzItemType.Buzz);
        }

        //// ----------------------------------------------------------------------------------------------------------

        [TestMethod]
        public void Constructor_ValueDivisibleByThreeAndFive_TypeEqualsFizzBuzz()
        {
            // Arrange
            const int Value = 15;
            var fizzBuzzItem = GetFizzBuzzItem(Value);

            // Act

            // Assert
            Assert.AreEqual(fizzBuzzItem.Type, FizzBuzzItemType.FizzBuzz);
        }

        //// ----------------------------------------------------------------------------------------------------------

        [TestMethod]
        public void GetDisplayValue_ValueNotDivisibleByThreeOrFive_ExpectDisplayValueOne()
        {
            // Arrange
            const int Value = 1;
            var fizzBuzzItem = GetFizzBuzzItem(Value);

            // Act
            var displayValue = fizzBuzzItem.GetDisplayValue(DayOfWeek.Friday);

            // Assert
            Assert.AreEqual("1", displayValue);
        }

        //// ----------------------------------------------------------------------------------------------------------

        [TestMethod]
        public void GetDisplayValue_ValueDivisibleByThree_ExpectDisplayValueFizz()
        {
            // Arrange
            const int Value = 3;
            var fizzBuzzItem = GetFizzBuzzItem(Value);

            // Act
            var displayValue = fizzBuzzItem.GetDisplayValue(DayOfWeek.Friday);

            // Assert
            Assert.AreEqual("Fizz", displayValue);
        }

        //// ----------------------------------------------------------------------------------------------------------

        [TestMethod]
        public void GetDisplayValue_ValueDivisibleByFive_ExpectDisplayValueBuzz()
        {
            // Arrange
            const int Value = 5;
            var fizzBuzzItem = GetFizzBuzzItem(Value);

            // Act
            var displayValue = fizzBuzzItem.GetDisplayValue(DayOfWeek.Friday);

            // Assert
            Assert.AreEqual("Buzz", displayValue);
        }

        //// ----------------------------------------------------------------------------------------------------------

        [TestMethod]
        public void GetDisplayValue_ValueDivisibleByThreeAndFive_ExpectDisplayValueFizzBuzz()
        {
            // Arrange
            const int Value = 15;
            var fizzBuzzItem = GetFizzBuzzItem(Value);

            // Act
            var displayValue = fizzBuzzItem.GetDisplayValue(DayOfWeek.Friday);

            // Assert
            Assert.AreEqual("FizzBuzz", displayValue);
        }

        //// ----------------------------------------------------------------------------------------------------------

        [TestMethod]
        public void GetDisplayValue_WednesdayValueDivisibleByThree_ExpectDisplayValueWizz()
        {
            // Arrange
            var fizzBuzzItem = GetFizzBuzzItem(3);

            // Act
            var displayValue = fizzBuzzItem.GetDisplayValue(DayOfWeek.Wednesday);

            // Assert
            Assert.AreEqual("Wizz", displayValue);
        }

        //// ----------------------------------------------------------------------------------------------------------

        [TestMethod]
        public void GetDisplayValue_TodayIsWednesdayValueDivisibleByFive_ExpectDisplayValueWuzz()
        {
            // Arrange
            var fizzBuzzItem = GetFizzBuzzItem(5);

            // Act
            var displayValue = fizzBuzzItem.GetDisplayValue(DayOfWeek.Wednesday);

            // Assert
            Assert.AreEqual("Wuzz", displayValue);
        }

        //// ----------------------------------------------------------------------------------------------------------

        private IFizzBuzzItem GetFizzBuzzItem(int number)
        {
            return new FizzBuzzItem(number);
        }

        //// ----------------------------------------------------------------------------------------------------------
    }
}