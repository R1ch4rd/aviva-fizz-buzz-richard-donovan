﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FizzBuzz.Models
{
    public class FizzBuzzModel
    {
        //// ----------------------------------------------------------------------------------------------------------

        private int pageNumber = 1;

        private int pageSize = 20;

        //// ----------------------------------------------------------------------------------------------------------

        [Range(1, 1000, ErrorMessage = "Value must be between 1 and 1000")]
        [Display(Name = "Value")]
        public int? Value { get; set; }

        //// ----------------------------------------------------------------------------------------------------------

        [Display(Name = "Fizz Buzz Items")]
        public List<FizzBuzzItemModel> FizzBuzzItems { get; set; }

        //// ----------------------------------------------------------------------------------------------------------

        public int PageNumber
        {
            get
            {
                return pageNumber;
            }
            set
            {
                pageNumber = value;
            }
        }

        //// ----------------------------------------------------------------------------------------------------------

        public int PageSize
        {
            get
            {
                return pageSize;
            }
            set
            {
                pageSize = value;
            }
        }
        
        //// ---------------------------------------------------------------------------------------------------------- 
    }
}