﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

using Domain;
using Domain.Repositories;

using FizzBuzz.Models;

namespace FizzBuzz.Controllers
{
    public class HomeController : Controller
    {
        //// ----------------------------------------------------------------------------------------------------------

        private IFizzBuzzItemRepository fizzBuzzItemRepository;

        //// ----------------------------------------------------------------------------------------------------------

        public HomeController(IFizzBuzzItemRepository repository)
        {
            fizzBuzzItemRepository = repository;
        }

        //// ----------------------------------------------------------------------------------------------------------
		 
        public ViewResult Index(FizzBuzzModel fizzBuzzModel)
        {
            if (fizzBuzzModel.Value == null)
                return View();

            if (ModelState.IsValid)
                try
                {
                    fizzBuzzModel.FizzBuzzItems = GetFizzBuzzItems(fizzBuzzModel.Value.Value);
                }
                catch (Exception)
                {
                    ModelState.AddModelError(String.Empty, "An error has occured with your request.");
                }

            return View(fizzBuzzModel);
        }

        //// ----------------------------------------------------------------------------------------------------------

        private List<FizzBuzzItemModel> GetFizzBuzzItems(int number)
        {
            var fizzBuzzItems = new List<IFizzBuzzItem>();

            for (int i = 1; i <= number; i++)
                fizzBuzzItems.Add(fizzBuzzItemRepository.Create(i));

            var modelItems = ConvertToModelItems(fizzBuzzItems);

            return modelItems;
        }

        //// ----------------------------------------------------------------------------------------------------------

        private List<FizzBuzzItemModel> ConvertToModelItems(List<IFizzBuzzItem> fizzBuzzItems)
        {
            var fizzBuzzItemModels = new List<FizzBuzzItemModel>();
            fizzBuzzItems.ForEach(
                fbi =>
                {
                    fizzBuzzItemModels.Add(new FizzBuzzItemModel { Type = fbi.Type, Value = fbi.GetDisplayValue(DateTime.Today.DayOfWeek) });
                });

            return fizzBuzzItemModels;
        }

        //// ---------------------------------------------------------------------------------------------------------- 
    }
}