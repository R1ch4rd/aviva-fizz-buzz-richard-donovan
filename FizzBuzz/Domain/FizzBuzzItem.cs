﻿using System;
using System.Collections;
using System.Diagnostics;

using Domain.Enums;

namespace Domain
{
    public class FizzBuzzItem : IFizzBuzzItem
    {
        //// ----------------------------------------------------------------------------------------------------------

        public int Value { get; set; }

        //// ----------------------------------------------------------------------------------------------------------

        public FizzBuzzItemType Type { get; set; }

        //// ----------------------------------------------------------------------------------------------------------
		 
        public FizzBuzzItem(int number)
        {
            Value = number;
            Type = SetType();
        }

        //// ----------------------------------------------------------------------------------------------------------

        private FizzBuzzItemType SetType()
        {
            var fizzBuzzItemType = FizzBuzzItemType.Number;

            if (Value % 15 == 0)
                fizzBuzzItemType = FizzBuzzItemType.FizzBuzz;
            else if (Value % 3 == 0)
                fizzBuzzItemType = FizzBuzzItemType.Fizz;
            else if (Value % 5 == 0)
                fizzBuzzItemType = FizzBuzzItemType.Buzz;

            return fizzBuzzItemType;
        }

        //// ----------------------------------------------------------------------------------------------------------
		 
        public string GetDisplayValue(DayOfWeek dayOfWeek)
        {
            switch (Type)
            {
                case FizzBuzzItemType.Fizz:
                    return dayOfWeek == DayOfWeek.Wednesday ? "Wizz" : "Fizz";

                case FizzBuzzItemType.Buzz:
                    return dayOfWeek == DayOfWeek.Wednesday ? "Wuzz" : "Buzz";

                case FizzBuzzItemType.FizzBuzz:
                    return "FizzBuzz";

                default:
                    return Value.ToString();
            }
        }

        //// ---------------------------------------------------------------------------------------------------------- 
    }
}