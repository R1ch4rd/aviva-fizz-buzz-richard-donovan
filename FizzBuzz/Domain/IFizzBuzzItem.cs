﻿using System;

using Domain.Enums;

namespace Domain
{
    public interface IFizzBuzzItem
    {
        //// ----------------------------------------------------------------------------------------------------------
		 
        int Value { get; set; }

        //// ----------------------------------------------------------------------------------------------------------
		 
        FizzBuzzItemType Type { get; set; }

        //// ----------------------------------------------------------------------------------------------------------

        string GetDisplayValue(DayOfWeek dayOfWeek);

        //// ---------------------------------------------------------------------------------------------------------- 
    }
}