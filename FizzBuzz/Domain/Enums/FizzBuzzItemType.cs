﻿namespace Domain.Enums
{
    public enum FizzBuzzItemType
    {
        Number = 1,
        Fizz = 2,
        Buzz = 3,
        FizzBuzz = 4
    }
}